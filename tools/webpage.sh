#!/bin/bash -e

# Generate the html files for www.cairographics.org/cairomm/ and
# the rest of www.cairographics.org
# or
# upload a cairomm webpage to cairographics.org/cairomm/.

# Run this script from the cairo-www directory where the source markdown code
# and (if built) the generated html files are.

USAGE="Usage: $0 [-g | -u <username>]"
if [ $# -lt 1 ]; then
  echo "$USAGE"
  exit 1
fi

UPLOAD_HOST=cairographics.org
DEST_DIR=dest

case "$1" in
-g) ikiwiki --url http://$UPLOAD_HOST/ --templatedir templates src $DEST_DIR
    ;;
-u) if [ $# -lt 2 ]; then
      echo "$USAGE"
      exit 1
    fi
    PACKAGE=cairomm
    USERNAME=$2
    UPLOAD_DIR=/srv/cairo.freedesktop.org/www/$PACKAGE
    FILENAME=index.html
    LOCAL_FILE=$DEST_DIR/$PACKAGE/$FILENAME
    REMOTE_DIR=$USERNAME@$UPLOAD_HOST:$UPLOAD_DIR

    # Download the presently published cairomm page.
    #scp $REMOTE_DIR/$FILENAME $LOCAL_FILE-old

    # Upload an updated cairomm page.
    scp $LOCAL_FILE $REMOTE_DIR
    ;;
*)  echo "$USAGE"
    exit 1
    ;;
esac
